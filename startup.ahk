; # = super
; ! = alt
; + = shift
; ^ = Ctrl

#b::
	Run, waterfox.exe
return

#f::
	Run, everything.exe
return

#+c::!F4

#m::WinMaximize, A

#o::#+Left

#enter::
if  !WinExist("ahk_exe WindowsTerminal.exe")
{run wt.exe}
WinActivate, ahk_exe WindowsTerminal.exe
return


#F11::
; reboot computer with WIN+F11
Run %comspec% /c "shutdown.exe /r /t 0"
;/r = restart
;/t 0 = wait 0 seconds
return

#F12::
; shutdown computer with WIN+F12
Run %comspec% /c "shutdown.exe /s /t 0"
;/s = shutdown
;/t 0 = wait 0 seconds
return